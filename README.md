# Personal Portfolio - Ivaylo Rashevski

## In order to run the app you need to install **Live Server** extention (Visual Studio Code)
## Or install live-server npm package by typing in terminal: *npm i -g live-server*
## Run it in root directory by typing in terminal: *live-server*

## *index.js* which can be found in the root of the project. The function *includeHTML* will be used for loading html files into *index.html*
## The decision was made, because we do not use any framework, but we try to break down
## The structure of the Web-site in sections/component as it would look if it was a real
## Task, real Web-app, developed with a framework.