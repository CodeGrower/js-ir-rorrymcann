import Book from './book.mjs';
import BookList from './booklist.mjs';
import HorrorBook from './horrorbook.mjs';

const listBookObj = new BookList();

const horrorBook = new HorrorBook('Horror Book', 'Novel', 'King', false, null, 0);
const horrorBook2 = new HorrorBook('Horror Book2', 'Novel', 'King', false, null, 0);
const firstBook = new Book('First Book', 'Novel', 'King', false, null);
const secondBook = new Book('Second Book', 'Novel', 'King', false, null);
const thirdBook = new Book('Third Book', 'Novel', 'King', false, null);
const fourthBook = new Book('Fourth Book', 'Novel', 'King', false, null);
const fifthBook = new Book('Fifth Book', 'Novel', 'King', false, null);

listBookObj.add(horrorBook);
listBookObj.add(horrorBook2);
listBookObj.add(firstBook);
listBookObj.add(secondBook);
listBookObj.add(thirdBook);
listBookObj.add(fourthBook);
listBookObj.add(fifthBook);

horrorBook.rate(horrorBook);
horrorBook.rate(horrorBook);
horrorBook.rate(horrorBook2);

listBookObj.setCurrentBook(thirdBook);
listBookObj.finishCurrentBook(thirdBook);
listBookObj.setCurrentBook(firstBook);
listBookObj.finishCurrentBook(firstBook);
console.log(listBookObj.allBooks);
console.log(listBookObj.nextBook);
console.log(listBookObj.currentBook);
console.log(listBookObj.lastBook);
console.log(listBookObj.booksRead);
console.log(listBookObj.booksToRead);