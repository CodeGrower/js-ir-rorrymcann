import  Book  from './book.mjs';

export default class HorrorBook extends Book {
  constructor(title, genre, author, read, readDate, rating) {
    super(title, genre, author, read, readDate);
    this.rating = rating;
  }

  rate(book) {
    if (book instanceof Book && !book.rating <= 5) {
        book.rating += 1;
    }
  }
}