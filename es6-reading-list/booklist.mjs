import  Book  from './book.mjs';

export default class BookList {
  constructor() {
    this.booksRead = 0;
    this.booksToRead = 0;
    this.nextBook = null;
    this.currentBook = null;
    this.lastBook = null;
    this.allBooks = [];
  }

  // Add new book to the array with all books.
   add(book) {
     const checkBook = this.allBooks.find(existingBook => existingBook.title === book.title);
     if (checkBook) {
       throw console.error('Book already exist!');
     }
     if (book instanceof Book) {
       this.booksToRead += 1;
       this.allBooks.push(book);
     }
  }

  /**
   * Function that accept Book object.
   * Set the book as currently read.
   * Place it at start position of all books.
   * Set next book from all books as next book to read.
   */
  setCurrentBook(incommingBook) {
    if (!incommingBook instanceof Book) {
      throw console.error('Please set valid book');
    }
    const findIncommingBook = this.allBooks.find(book => book.title === incommingBook.title);

    if(!incommingBook) {
      throw console.error('Book is not in the list!');
    }

    this.allBooks = this.allBooks.filter(book => book.title !== incommingBook.title);
  
    this.nextBook = this.allBooks[0];
    
    const updatedBook = { ...findIncommingBook, read: true };
    this.allBooks.unshift(updatedBook);
    
    this.currentBook = this.allBooks[0];
  }

  /**
   * Function that accept Book object.
   * Set it as finished book - already not currently reading.
   * Set Date when user finished reading the book.
   * Set the book as last readed book from the collenction of books and move it to the end of the collection.
   */
  finishCurrentBook(incommingBook) {
    if (!incommingBook instanceof Book) {
      throw console.error('Please set valid book');
    }
    const updatedBook = { ...incommingBook, readDate: new Date(Date.now()), read: false }
    
    this.allBooks = this.allBooks.filter(book => book.title !== incommingBook.title);
    this.allBooks.push(updatedBook);

    this.lastBook = this.allBooks.slice(-1);
    this.currentBook = null;

    this.booksRead += 1;
    this.booksToRead -= 1;
  }
}