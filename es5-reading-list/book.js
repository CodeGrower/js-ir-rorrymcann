var validateParamsLength = require('./validate.js');

function Book (title, genre, author, read, readDate) {
  validateParamsLength(title, 2, 15);
  validateParamsLength(genre, 5, 10);
  validateParamsLength(author, 3, 14);
  this.title = title;
  this.genre = genre;
  this.author = author;
  this.read = read;
  this.readDate = readDate;
}

module.exports = Book;