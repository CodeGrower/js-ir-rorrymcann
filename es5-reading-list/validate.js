var validateParamsLength = function (param, minLength, maxLength) {
  if (!param instanceof String) {
    throw console.error('Please validate Strings only!');
  }
  if (param.length < minLength) {
    throw console.error('Lack of characters -> ' + param);
  }
  if (param.length > maxLength) {
    throw console.error('Too many characters -> ' + param);
  }
}

module.exports = validateParamsLength;