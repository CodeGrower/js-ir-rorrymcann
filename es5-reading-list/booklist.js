function BookList () {
  this.booksRead = 0;
  this.booksToRead = 0;
  this.nextBook = null;
  this.currentBook = null;
  this.lastBook = null;
  this.allBooks = [];
}

BookList.prototype.add = function (book) {
  this.booksToRead += 1;
  this.allBooks.push(book);
}

BookList.prototype.setCurrentBook = function (incommingBook) {
  var findIncommingBook = JSON.parse(JSON.stringify(incommingBook));

  if(!incommingBook) {
    throw console.error('Book is not in the list!');
  }

  this.allBooks = this.allBooks.filter(book => book.title !== incommingBook.title);

  this.nextBook = this.allBooks[0];

  findIncommingBook.read = true;
  this.allBooks.unshift(findIncommingBook);
  this.currentBook = this.allBooks[0];
}

BookList.prototype.finishCurrentBook = function (incommingBook) {
  var updatedBook = JSON.parse(JSON.stringify(incommingBook));

  updatedBook.readDate = new Date(Date.now());
  updatedBook.read = false;

  this.allBooks = this.allBooks.filter(book => book.title !== incommingBook.title);
  this.allBooks.push(updatedBook);

  this.lastBook = this.allBooks.slice(-1);
  this.currentBook = null;

  this.booksRead += 1;
  this.booksToRead -= 1;
}

module.exports = BookList;