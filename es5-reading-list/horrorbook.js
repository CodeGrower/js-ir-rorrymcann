var Book = require('./book.js');

function HorrorBook (title, genre, author, read, readDate) {
  Book.call(this, title, genre, author, read, readDate);
  this.rating = 0;
}

HorrorBook.prototype = Object.create(Book.prototype);
HorrorBook.prototype.constructor = HorrorBook;

HorrorBook.prototype.rate = function (book) {
  if (book instanceof Book && !book.rating <= 5){
      book.rating += 1;
  }
}

module.exports = HorrorBook;