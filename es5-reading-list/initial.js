var Book = require('./book.js');
var HorrorBook = require('./horrorbook.js');
var BookList = require('./booklist.js');

var listBookObj = new BookList();

var horrorBook = new HorrorBook('Horror Book', 'Novel', 'King', false, null, 0);
var horrorBook2 = new HorrorBook('Horror Book2', 'Novel', 'King', false, null, 0);
var firstBook = new Book('First Book', 'Novel', 'King', false, null);
var secondBook = new Book('Second Book', 'Novel', 'King', false, null);
var thirdBook = new Book('Third Book', 'Novel', 'King', false, null);
var fourthBook = new Book('Fourth Book', 'Novel', 'King', false, null);
var fifthBook = new Book('Fifth Book', 'Novel', 'King', false, null);

listBookObj.add(horrorBook);
listBookObj.add(horrorBook2);
listBookObj.add(firstBook);
listBookObj.add(secondBook);
listBookObj.add(thirdBook);
listBookObj.add(fourthBook);
listBookObj.add(fifthBook);

horrorBook.rate(horrorBook);
horrorBook.rate(horrorBook2);
horrorBook2.rate(horrorBook);
horrorBook2.rate(horrorBook);
horrorBook2.rate(horrorBook);

listBookObj.setCurrentBook(thirdBook);
listBookObj.finishCurrentBook(thirdBook);
listBookObj.setCurrentBook(firstBook);
listBookObj.finishCurrentBook(firstBook);
console.log(listBookObj.allBooks);
console.log(listBookObj.nextBook);
console.log(listBookObj.currentBook);
console.log(listBookObj.lastBook);
console.log(listBookObj.booksRead);
console.log(listBookObj.booksToRead);

console.log(listBookObj.allBooks);